# freeCodeCampJpn
![cover_pic](cover.png)

[freeCodeCamp](https://www.freecodecamp.org)の日本語訳です。

本レポジトリーは、 [https://github.com/freeCodeCamp/freeCodeCamp](https://github.com/freeCodeCamp/freeCodeCamp) です。

## 目的

freeCodeCampを日本で普及させること

## 手段

freeCodeCampを用いて、無料のWeb研修会を実施します。

## 開発環境

macOS

## 手順

1. [Node.js](http://nodejs.org)(`v8.9.3`)のインストール
2. [MongoDB](https://docs.mongodb.com/manual/administration/install-community/)(`v3.6.3`)のインストール
3. [MailHog](https://github.com/mailhog/MailHog)のインストール
4. freeCodeCampのインストール
5. freeCodeCampの起動
6. 翻訳
7. 学校やサークル等のLANで運用する

### 1. Nodejsのインストール

nvmでnodejs環境を構築します。

```
$ curl -o- https://raw.githubusercontent.com/creationix/nvm/v0.33.8/install.sh | bash
$ nvm install 8.9.3
$ node -v #v8.9.3
$ npm -v #5.5.1
```

### 2. MongoDBのインストール

```
$ brew install mongodb
$ mongod -version # v3.6.3
$ sudo chmod 777 /data/db # データ用のフォルダに権限を与えます。
```

### 3. Mailhogのインストール

```
$ brew install mailhog
$ brew services start mailhog
```

### 4. freeCodeCampのインストール

ソースコードを入手して、デイレクトリを移動します。

```bash
$ git clone https://github.com/yourUsername/freeCodeCamp.git
$ cd freeCodeCamp
```

JavaScriptの依存ライブラリをインストールします。
```bash
$ npm install
```

### 5. freeCodeCampの起動

mongoサーバを起動します。

```bash
$ sudo killall -15 mongod
$ sudo mongod
```

クイズのデータをmongodbに読み込みます。

```bash
$ npm run only-once
```

```bash
$ npm run develop
```

Firefoxで [http://localhost:3000](http://localhost:3000) にアクセスします。

### 6. 翻訳

`seed/challenges/`の問題データの`jpn`に、日本語訳を加えます。

`title`にタイトル、`description`に内容を記入してください。

(例) `seed/challenges/02-javascript-algorithms-and-data-structures/basic-javascript.json`

```
    {
      "id": "bd7123c9c441eddfaeb4bdef",
      "title": "Comment Your JavaScript Code",
      "description": [
        "Comments are lines of code that JavaScript will intentionally ignore."
      ],
      "challengeSeed": [
        ""
      ],
      "solutions": [
        "// Fake Comment\n/* Another Comment */"
      ],
      "tests": [
        "assert(code.match(/(\\/\\/)...../g), 'message: Create a <code>//</code> style comment that contains at least five letters.');",
        "assert(code.match(/(\\/\\*)([^\\*\\/]{5,})(?=\\*\\/)/gm), 'message: Create a <code>/* */</code> style comment that contains at least five letters.');"
      ],
      "type": "waypoint",
      "challengeType": 1,
      "translations": {
        "jpn": {
          "title": "コメントを書きます。",
          "description": [
            "コメントは、JavaScriptが実行せずに無視する行のことをいいます。"
          ]
        }
      }
    },
```

データの変更を、MongoDBに反映させます。

```bash
$ node seed
```

freeCodeCampを開始します。

```
$ npm run develop
```

翻訳のデータにコメントを記入するときには、

```
"<code>/*</code>"

"<blockquote>/* This is a <br>   multi-line comment */</blockquote>"
```
のように、コメント記号にtagをつける必要があります。tagをつけないとパースエラーになります。

### 7. 学校やサークル等のLANで運用する

まず、教師PCのIPアドレスを`ifconfig`等のコマンドで確認します。

(教師PCのIPアドレスが`192.168.100.10`だと仮定して、以下の説明を進めます。)

freeCodeCampを開始します。

```
$ npm run develop
```

Mapを押すと、全ての学習コースを学習できます。

もし、学習履歴を残すには、`Sigh Up`から学習者を登録します。

メールアドレスを記入して送信後、[http://192.168.100.10:8025](http://192.168.100.10:8025)にアクセスすると、Mailhogの画面でユーザー情報を入手できます。

次のようなキーが発行されるので、学習者に送付してあげてください。

学習者のブラウザからこのURIにアクセスすると、ユーザ登録ができて、学習履歴を残せます。

`http://192.168.100.10:3000/passwordless-auth/?email=cGFkZHMxc3RAZ21haWwuY29t&token=CMdcXfWzoCueKFbfbaE7LKU7mV7lzgdjmEAKCZNFVQOD7yyTjrpjf4ra2E5cVlbB`

### 8. Pull Request

Pull Requestで翻訳を送っていただけると、内容を確認してmergeします。

Pull Requestを送っていただく前に、`node seed`をして、データが正しくパースできるか確認してください。

よろしくお願いします。
